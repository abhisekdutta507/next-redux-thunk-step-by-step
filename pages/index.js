import { shallowEqual, useSelector, useDispatch } from "react-redux";
import { counter as counterActions, asyncCounter as asyncCounterActions } from "../store/actions";
import styles from "../styles/Home.module.css";

export default function Home() {
  const { counter, asyncCounter } = useSelector((state) => state, shallowEqual);
  const dispatch = useDispatch();

  const increment = () => {
    dispatch(counterActions.increment(1));
  };

  const decrement = () => {
    dispatch(counterActions.decrement(1));
  };

  const asyncIncrement = () => {
    dispatch(asyncCounterActions.increment(1));
  }

  const asyncDecrement = () => {
    dispatch(asyncCounterActions.decrement(1));
  }

  return (
    <div className={styles.container}>
      <p className={`${styles.headerText} header-text m-0 p-1`}>Welcome to Next.js Redux Example</p>

      <div className="flex col">
        <p className={`${styles.displayLabel} m-0 p-1`}>Sync counter <span className={`${styles.displayLabelReflected}`}>{counter}</span></p>
        <p className={`${styles.displayLabel} m-0 p-1`}>Async counter <span className={`${styles.displayLabelReflected}`}>{asyncCounter?.data} {asyncCounter?.loading ? 'Loading...' : ''}</span></p>
        <div className="flex p-1">
          <button className="f-18" onClick={increment}>Increment +</button>
          <button className="f-18" onClick={decrement}>Decrement -</button>
          <button className="f-18" onClick={asyncIncrement}>Increment Async +</button>
          <button className="f-18" onClick={asyncDecrement}>Async Decrement -</button>
        </div>
      </div>
    </div>
  );
}
