const wait = (n = 1) => {
  return new Promise((next) => setTimeout(() => next(true), 1000 * n));
};

export default wait;
