This project is created with [nextjs.org](https://nextjs.org/docs/getting-started).

Press  `Cmd + Shift + V`  form your Mac keyboard to preview the README.md.

## Available Scripts

In the project directory, you can run:

### `npm run dev`

Starts the project in development mode.

### `npm i`

Install all project related dependency.

### Setup React-Redux on Next.js project

**Initialize** the Next.js project with the below command.

```bash
npx create-next-app@latest
```

**Add dependencies** for [react-redux](https://react-redux.js.org/tutorials/quick-start).

```bash
npm i redux react-redux redux-thunk
```

**Create the reducer** file [counter.reducer.js](store/reducers/counter.reducer.js).

```js
const counter = (state = 0, action = { type: '', payload: 0 }) => {
  switch (action.type) {
    case 'INCREMENT': return state + action.payload;
    case 'DECREMENT': return state - action.payload;
    default: return state;
  }
}

export default counter;
```

**Add the first async reducer** file [asyncCounter.reducer.js](store/reducers/asyncCounter.reducer.js).

```js
import {
  ASYNC_INCREMENT_REQUEST,
  ASYNC_DECREMENT_REQUEST,
  ASYNC_INCREMENT,
  ASYNC_DECREMENT,
} from '../constants';

const initialState = {
  loading: false,
  data: 0,
};

const asyncCounter = (
  state = initialState,
  action = { type: '', payload: 0 }
) => {
  switch (action.type) {
    case ASYNC_INCREMENT_REQUEST: {
      return { ...state, loading: true };
    }
    case ASYNC_DECREMENT_REQUEST: {
      return { ...state, loading: true };
    }
    case ASYNC_INCREMENT: {
      return { loading: false, data: state.data + action.payload };
    }
    case ASYNC_DECREMENT: {
      return { loading: false, data: state.data - action.payload };
    }
    default: return state;
  }
};

export default asyncCounter;
```

**Let's add an index reducer** file [index.js](store/reducers/index.js).

```js
import { combineReducers } from 'redux';
import counter from './counter.reducer';
import asyncCounter from './asyncCounter.reducer';

const reducers = combineReducers({
  counter,
  asyncCounter
});

export default reducers;
```

**Add the first action** file [counter.actions.js](store/actions/counter.actions.js).

```js
export const increment = (n = 1) => {
  return {
    type: 'INCREMENT',
    payload: n
  }
}

export const decrement = (n = 1) => {
  return {
    type: 'DECREMENT',
    payload: n
  }
}
```

**Add the first async action** file [asyncCounter.actions.js](store/actions/asyncCounter.actions.js).

```js
export const asyncIncrementRequest = () => {
  return {
    type: ASYNC_INCREMENT_REQUEST
  }
}

export const asyncIncrement = (n = 1) => {
  return {
    type: ASYNC_INCREMENT,
    payload: n
  }
}

export const increment = (n = 1) => {
  return async (dispatch = () => {}) => {
    dispatch(asyncIncrementRequest());
    // toggle loading
    await wait(2);
    dispatch(asyncIncrement(n));
  }
}

// ...
```

**Now add an index action** file [index.js](store/actions/index.js).
```js
import * as counter from './counter.action'
import * as asyncCounter from './asyncCounter.actions'

export {
  counter,
  asyncCounter
};
```

**Let's add a store index** file [index.js](store/index.js).

```js
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

const composeEnhancers = global.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const middlewares = [thunk];
const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);

// STORE -> GLOBALIZED STATE
const store = createStoreWithMiddleware(reducers, composeEnhancers());

store.subscribe(() => {});

export default store;
```

**The final step** is to add the provider on [_app.js](pages/_app.js).

```js
import { Provider } from 'react-redux';
import store from '../store';
import '../styles/globals.css'
function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  )
}
export default MyApp
```

**Start using the store** in [index.js](pages/index.js).

```js
import { shallowEqual, useSelector, useDispatch } from "react-redux";
import { counter as counterActions, asyncCounter as asyncCounterActions } from "../store/actions";
import styles from "../styles/Home.module.css";

export default function Home() {
  const { counter, asyncCounter } = useSelector((state) => state, shallowEqual);
  const dispatch = useDispatch();

  const increment = () => {
    dispatch(counterActions.increment(1));
  };

  const decrement = () => {
    dispatch(counterActions.decrement(1));
  };

  const asyncIncrement = () => {
    dispatch(asyncCounterActions.increment(1));
  }

  const asyncDecrement = () => {
    dispatch(asyncCounterActions.decrement(1));
  }

  return (
    <div className={styles.container}>
      <p className={`${styles.headerText} header-text m-0 p-1`}>Welcome to Next.js Redux Example</p>

      <div className="flex col">
        <p className={`${styles.displayLabel} m-0 p-1`}>Sync counter <span className={`${styles.displayLabelReflected}`}>{counter}</span></p>
        <p className={`${styles.displayLabel} m-0 p-1`}>Async counter <span className={`${styles.displayLabelReflected}`}>{asyncCounter?.data} {asyncCounter?.loading ? 'Loading...' : ''}</span></p>
        <div className="flex p-1">
          <button className="f-18" onClick={increment}>Increment +</button>
          <button className="f-18" onClick={decrement}>Decrement -</button>
          <button className="f-18" onClick={asyncIncrement}>Increment Async +</button>
          <button className="f-18" onClick={asyncDecrement}>Async Decrement -</button>
        </div>
      </div>
    </div>
  );
}
```

Hurray!! we have learned the basic concepts of Redux Thunk.
