import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

const composeEnhancers = global.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const middlewares = [thunk];
const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);

// STORE -> GLOBALIZED STATE
const store = createStoreWithMiddleware(reducers, composeEnhancers());

store.subscribe(() => {});

export default store;
