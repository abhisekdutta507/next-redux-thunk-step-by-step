import { combineReducers } from 'redux';
import counter from './counter.reducer';
import asyncCounter from './asyncCounter.reducer';

const reducers = combineReducers({
  counter,
  asyncCounter
});

export default reducers;
