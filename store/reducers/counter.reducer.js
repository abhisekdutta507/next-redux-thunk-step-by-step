import { INCREMENT, DECREMENT } from '../constants';

const initialState = 0;

const counter = (state = initialState, action = { type: '', payload: 0 }) => {
  switch (action.type) {
    case INCREMENT: return state + action.payload;
    case DECREMENT: return state - action.payload;
    default: return state;
  }
}

export default counter;
