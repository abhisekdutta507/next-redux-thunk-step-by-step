import {
  ASYNC_INCREMENT_REQUEST,
  ASYNC_DECREMENT_REQUEST,
  ASYNC_INCREMENT,
  ASYNC_DECREMENT,
} from '../constants';

const initialState = {
  loading: false,
  data: 0,
};

const asyncCounter = (
  state = initialState,
  action = { type: '', payload: 0 }
) => {
  switch (action.type) {
    case ASYNC_INCREMENT_REQUEST: {
      return { ...state, loading: true };
    }
    case ASYNC_DECREMENT_REQUEST: {
      return { ...state, loading: true };
    }
    case ASYNC_INCREMENT: {
      return { loading: false, data: state.data + action.payload };
    }
    case ASYNC_DECREMENT: {
      return { loading: false, data: state.data - action.payload };
    }
    default: return state;
  }
};

export default asyncCounter;
