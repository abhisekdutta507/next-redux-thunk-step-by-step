import { ASYNC_INCREMENT_REQUEST, ASYNC_DECREMENT_REQUEST, ASYNC_INCREMENT, ASYNC_DECREMENT } from '../constants';
import { wait } from '../../utils';

export const asyncIncrementRequest = () => {
  return {
    type: ASYNC_INCREMENT_REQUEST
  }
}

export const asyncDecrementRequest = () => {
  return {
    type: ASYNC_DECREMENT_REQUEST
  }
}

export const asyncIncrement = (n = 1) => {
  return {
    type: ASYNC_INCREMENT,
    payload: n
  }
}

export const asyncDecrement = (n = 1) => {
  return {
    type: ASYNC_DECREMENT,
    payload: n
  }
}

export const increment = (n = 1) => {
  return async (dispatch = () => {}) => {
    dispatch(asyncIncrementRequest());
    // toggle loading
    await wait(2);
    dispatch(asyncIncrement(n));
  }
}

export const decrement = (n = 1) => {
  return async (dispatch = () => {}) => {
    dispatch(asyncDecrementRequest());
    // toggle loading
    await wait(2);
    dispatch(asyncDecrement(n));
  }
}
